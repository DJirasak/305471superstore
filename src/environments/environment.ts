// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCFgoKU51ykCdtHtNx0HTEt1w4jkwX6A-w",
    authDomain: "superstore-8f730.firebaseapp.com",
    databaseURL: "https://superstore-8f730.firebaseio.com",
    projectId: "superstore-8f730",
    storageBucket: "superstore-8f730.appspot.com",
    messagingSenderId: "618559055025",
    appId: "1:618559055025:web:ecfb589b6807f4e77cae90",
    measurementId: "G-CCJC39H6K4"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
