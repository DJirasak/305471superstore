import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

import * as  Highcharts from 'highcharts';
import  More from 'highcharts/highcharts-more';
More(Highcharts);
import Drilldown from 'highcharts/modules/drilldown';
Drilldown(Highcharts);
// Load the exporting module.
import Exporting from 'highcharts/modules/exporting';
import { HttpClient } from '@angular/common/http';

// Initialize exporting module.
Exporting(Highcharts);



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

   

  @ViewChild("container" ,{read: ElementRef, static: true} )container: ElementRef;
  
  constructor(private http:HttpClient) { }

  ngOnInit() {
    this.http.get('../../assets/ExportGDP2020.json').subscribe(data => {
      const userStr = JSON.stringify(data);
      this.http.get('../../assets/gdp_2018CL.json').subscribe(data2 => {
        const userStr2 = JSON.stringify(data2);
        const userStr2J = JSON.parse(userStr2);
       
    


    Highcharts.chart(this.container.nativeElement, {
    // Created pie chart using Highchart
    chart :{
      type: 'pie'
    },
    title: {
      text: 'การคาดการณ์ค่า GDP ของประเทศต่าง ๆ ในปี 2020'
  },
  subtitle: {
      text: 'GDP : <a href="https://th.wikipedia.org/wiki/%E0%B8%9C%E0%B8%A5%E0%B8%B4%E0%B8%95%E0%B8%A0%E0%B8%B1%E0%B8%93%E0%B8%91%E0%B9%8C%E0%B8%A1%E0%B8%A7%E0%B8%A5%E0%B8%A3%E0%B8%A7%E0%B8%A1%E0%B9%83%E0%B8%99%E0%B8%9B%E0%B8%A3%E0%B8%B0%E0%B9%80%E0%B8%97%E0%B8%A8">Wikipedia</a>'
  },
  xAxis: {
      type: 'category',
      labels: {
          rotation: -90,
          style: {
              fontSize: '3px',
              fontFamily: 'Verdana, sans-serif'
          }
      }
  },
  yAxis: {
      min: 10000,
      max:40676229442861,
      title: {
          text: 'GDP (USD)'
      }
  },
  legend: {
      enabled: false
  },
  plotOptions: {
    column: {
        pointPadding: 0,
        borderWidth: 0,
        groupPadding: 0,
        shadow: true
    }
},
  tooltip: {
      pointFormat: '<b>{point.y:.2f} USD</b>'
  },
  series: [{
      name: 'Country Name',
      colorByPoint: true,
      data: JSON.parse(userStr)
    }],
    drilldown: {
      series: userStr2J
    }
  });
}); 
}); 
  }
}
